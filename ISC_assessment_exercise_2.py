class vending_machine:
    # the available items in the vending machine and their prices
    inventory = {
        "Sprite": 1.50,
        "Coke": 1.50,
        "Water": 1.00,
        "Funyuns": 1.25,
        "Skittles": 1.00
    }

    # this method displays the items in the vending machine
    def display_items(self):
        print("Welcome to Krystopher's vending machine!")
        print("Here are the available items:")
        for item, price in self.inventory.items():
            print(f"{item}: ${price:.2f}")

    # this method adds money to the vending machine
    def add_money(self, balance):
        while True:
            try:
                amount = float(input("Enter the amount of money to add: "))
                if amount <= 0:
                    raise ValueError
                balance += amount
                print(f"Added ${amount:.2f}. Your new balance is ${balance:.2f}.")
                return balance
            except ValueError:
                print("Invalid input. Please enter a positive number.")

    # this method makes a purchase from the vending machine
    def make_purchase(self, balance):
        while True:
            try:
                item = input("Enter the item you want to purchase: ")
                if item not in self.inventory:
                    raise ValueError
                price = self.inventory[item]
                if balance < price:
                    raise ValueError
                balance -= price
                print(f"Purchased {item} for ${price:.2f}.")
                if balance > 0:
                    quarters, dimes, nickels, pennies = self.return_change(balance)
                    print(f"Your change is {quarters} quarters, {dimes} dimes, {nickels} nickels, and {pennies} pennies.")
                return 0
            except ValueError:
                print("Invalid input or insufficient funds.")

    # this method returns your change
    def return_change(self, amount):
        quarters = int(amount // 0.25)
        amount -= quarters * 0.25
        dimes = int(amount // 0.10)
        amount -= dimes * 0.10
        nickels = int(amount // 0.05)
        amount -= nickels * 0.05
        pennies = int(amount // 0.01)
        return (quarters, dimes, nickels, pennies)

    # this is the main method of the program
    def main(self):
        balance = 0
        self.display_items()
        while True:
            print(f"Your current balance is ${balance:.2f}.")
            action = input("Enter 'add' to add money or 'purchase' to make a purchase: ")
            if action == "add":
                balance = self.add_money(balance)
            elif action == "purchase":
                balance = self.make_purchase(balance)
            else:
                print("Invalid input. Please try again.")

# call the main method to start the program
if __name__ == "__main__":
    vmachine = vending_machine() 
    vmachine.main()
