import pyathena
import pandas as pd
from urllib.parse import quote_plus
from sqlalchemy.engine import create_engine
import os
from io import StringIO
import boto3
import s3fs
import datetime
from datetime import date
from datetime import timedelta
import json
import numpy as np


class _feed:
        ssm = boto3.client('ssm', 'us-east-1')
        s3_client = boto3.client('s3')
        sns_client = boto3.client('sns', 'us-east-1')
        sts_client = boto3.client('sts', 'us-east-1')

        account_id = sts_client.get_caller_identity()["Account"]


        today = date.today()
        yesterday = today - timedelta(days = 1)
        year = yesterday.year
        month = yesterday.month
        day=yesterday.day

        current_time = datetime.datetime.now()

        athena_access_key = ssm.get_parameter(
                Name='ATHENA_AWS_ACCESS_KEY',
                WithDecryption=True
        )

        athena_secret_key = ssm.get_parameter(
                Name='ATHENA_AWS_SECRET_KEY',
                WithDecryption=True
        )
        athena_conn = pyathena.connect(aws_access_key_id=athena_access_key.get("Parameter").get("Value"), ##credentials of aws_access_key_id
                aws_secret_access_key=athena_secret_key.get("Parameter").get("Value"), ##credentials of aws_secret_access_key
                s3_staging_dir='s3://prod.eu-central-1.external-user.athena/athena-results/', ##where the athena query result saved - checked in S3 ,
                region_name='eu-central-1') ##the region you set for Athena

	def inventory(self, date="current_date - interval '1' Day"):
                try:

                   df = pd.read_sql(f"SELECT * FROM inventory WHERE _date = {date}", self.athena_conn)
                   csv_buffer = StringIO()
                   if df.empty == False:
                        new_frame = df[['creationdatetime','filename','_date','vehicle_vin','vehicle_datasource',
                        'vehicle_modelyear','vehicle_ordertype','vehicle_exteriorcolorcode','vehicle_interiorcolorcode',
                        'vehicle_cnumber','vehicle_previousowners','vehicle_accidentfree','vehicle_usagehistory','vehicle_vehiclecondition']]

                        new_frame.to_csv(csv_buffer, sep='|')
                        self.s3_client.put_object(Bucket='file-upload', Key=f'inventory/report_inventory/inventory_{self.year}{self.month}{self.day}_{self.current_time}.csv', Body=csv_buffer.getvalue(), ServerSideEncryption ="aws:kms")
                        resp = self.sns_client.publish(TargetArn=f"arn:aws:sns:us-east-1:{self.account_id}:file-load-alerts", Message=json.dumps("The process for loading inventory file to S3 is complete. All files can be found in the file-upload S3 bucket. If you have any questions please reach out to your AWS admin."), Subject="INFORMATION: File Load Complete")
                   else:
                        self.s3_client.put_object(Bucket='file-upload', Key=f'file_reload/report_inventory_{self.year}-{self.month}-{self.day}.csv', Body=csv_buffer.getvalue(), ServerSideEncryption ="aws:kms")
                        resp = self.sns_client.publish(TargetArn=f"arn:aws:sns:us-east-1:{self.account_id}:file-load-alerts", Message=json.dumps("The inventory table contains 0 records. Please reach out to your AWS admin for further assistance"), Subject="WARNING: Empty Data Frame")
                except Exception as e:
                   resp = self.sns_client.publish(TargetArn=f"arn:aws:sns:us-east-1:{self.account_id}:file-load-alerts", Message=json.dumps(f"The inventory file could not be loaded, please see the following error for more details: {e}. If you have any questions please reach out to your AWS admin."),
                   Subject="ERROR: OCS File Load Incomplete")

if __name__ == "__main__":
        feed = _feed()
        feed.inventory()
